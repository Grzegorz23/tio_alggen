﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace GenAlgComi
{
    class Program
    {
        static void Main(string[] args)
        {
            // Create and add our cities
            City city = new City(60, 200);
            TourManagement.addCity(city);
            City city2 = new City(180, 200);
            TourManagement.addCity(city2);
            City city3 = new City(80, 180);
            TourManagement.addCity(city3);
            City city4 = new City(140, 180);
            TourManagement.addCity(city4);
            City city5 = new City(20, 160);
            TourManagement.addCity(city5);
            City city6 = new City(100, 160);
            TourManagement.addCity(city6);
            City city7 = new City(200, 160);
            TourManagement.addCity(city7);
            City city8 = new City(140, 140);
            TourManagement.addCity(city8);
            City city9 = new City(40, 120);
            TourManagement.addCity(city9);
            City city10 = new City(100, 120);
            TourManagement.addCity(city10);
            City city11 = new City(180, 100);
            TourManagement.addCity(city11);
            City city12 = new City(60, 80);
            TourManagement.addCity(city12);
            City city13 = new City(120, 80);
            TourManagement.addCity(city13);
            City city14 = new City(180, 60);
            TourManagement.addCity(city14);
            City city15 = new City(20, 40);
            TourManagement.addCity(city15);
            City city16 = new City(100, 40);
            TourManagement.addCity(city16);
            City city17 = new City(200, 40);
            TourManagement.addCity(city17);
            City city18 = new City(20, 20);
            TourManagement.addCity(city18);
            City city19 = new City(60, 20);
            TourManagement.addCity(city19);
            City city20 = new City(160, 20);
            TourManagement.addCity(city20);
            City city21 = new City(1600, 20);
            TourManagement.addCity(city21);
            City city22 = new City(1850, 60);
            TourManagement.addCity(city22);
            City city23 = new City(120, 44);
            TourManagement.addCity(city23);
            City city24 = new City(174, 44);
            TourManagement.addCity(city24);
            City city25 = new City(250, 66);
            TourManagement.addCity(city25);
            City city26 = new City(278, 33);
            TourManagement.addCity(city26);
            City city27 = new City(610, 28);
            TourManagement.addCity(city27);
            City city28 = new City(182, 26);
            TourManagement.addCity(city28);
            City city29 = new City(1600, 25);
            TourManagement.addCity(city29);
            // Initialize population
            Population pop = new Population(150, true);
            WriteLine("Initial distance: " + pop.FindBestTour().getDistance());

            // Evolve population for 100 generations
            pop = GenAlg.evolve(pop);
            for (int i = 0; i < 150; i++)
            {
                pop = GenAlg.evolve(pop);
                //WriteLine(pop.AvgFitness());
                WriteLine(pop.FindBestTour().getDistance());
            }

            // Print final results
            WriteLine("Finished");
            WriteLine("Final distance: " + pop.FindBestTour().getDistance());
            WriteLine("Solution:");
            WriteLine(pop.FindBestTour());
            ReadLine();
        }


    }
}
