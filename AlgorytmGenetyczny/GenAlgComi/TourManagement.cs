﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenAlgComi
{
    class TourManagement
    {
        private static List<City> destinations = new List<City>();

        public static void addCity(City city)
        {
            destinations.Add(city);
        }

        public static City getCityOnPosition(int idx)
        {
            if (idx < destinations.Count)
            {
                return destinations[idx];
            }
            else
            {
                Console.WriteLine("Out of bounds of destinations");
                return null;
            }
        }

        public static int destinationsCount()
        {
            return destinations.Count;
        }
    }
}

