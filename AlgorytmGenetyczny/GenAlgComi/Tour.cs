﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Linq;

namespace GenAlgComi
{
    class Tour
    {

        public List<City> tour = new List<City>();
        double fitness = 0;
        double distance = 0;


        public Tour()
        {
            for (int i = 0; i < TourManagement.destinationsCount(); i++)
            {
                tour.Add(null);
            }
        }

        public Tour(List<City> tour)
        {
            this.tour = tour;
        }

        public bool containsCity(City city)
        {
            return tour.Contains(city);
        }

        public City GetTourCity(int pos)
        {
            return tour[pos];
        }

        private List<E> ShuffleList<E>(List<E> inputList)
        {
            List<E> randomList = new List<E>();

            Random r = new Random();
            int randomIndex = 0;
            while (inputList.Count > 0)
            {
                randomIndex = r.Next(0, inputList.Count);
                randomList.Add(inputList[randomIndex]);
                inputList.RemoveAt(randomIndex);
            }

            return randomList;
        }



        public void generateTour()
        {
            for (int cIdx = 0; cIdx < TourManagement.destinationsCount(); cIdx++)
            {
                setTourCity(cIdx, TourManagement.getCityOnPosition(cIdx));
            }
            tour = ShuffleList(tour);

        }

        public void setTourCity(int pos, City c)
        {
            tour[pos] = c;
            fitness = 0;
            distance = 0;
        }

        public double getDistance()
        {
            if (distance == 0)
            {
                double tourDistance = 0;
                // Loop through our tour's cities
                for (int cityIndex = 0; cityIndex < tour.Count; cityIndex++)
                {
                    City fromCity = GetTourCity(cityIndex);
                    City destinationCity;
                    if (cityIndex + 1 < tour.Count)
                    {
                        destinationCity = GetTourCity(cityIndex + 1);
                    }
                    else
                    {
                        destinationCity = GetTourCity(0);
                    }
                    tourDistance += fromCity.DistanceToCity(destinationCity);
                }
                distance = tourDistance;
            }
            return distance;
        }

        public double Fitness()
        {
            if (fitness == 0)
            {
                fitness = 1 / getDistance();
            }
            return fitness;
        }

        public int TourSize()
        {
            return tour.Count;
        }

        public override string ToString()
        {
            base.ToString();
            string ret = "|";
            tour.ForEach(x => ret += x + "|");
            return ret;
        }

    }
}

