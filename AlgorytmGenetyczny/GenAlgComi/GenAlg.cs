﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenAlgComi
{
    class GenAlg
    {
        static double mutationFactor = 0.6;
        static double crossingFactor = 0.8;
        static int tournamentSize = 15;
        static bool saveBestIndividual = true;
        private static Random rand = new Random();

        public static Population evolve(Population pop)
        {
            //bez inicjowania nowych osobników
            Population newGeneration = new Population(pop.PopulationSize(), false);

            int bestOffset = 0;
            if (saveBestIndividual)
            {
                newGeneration.saveTour(0, pop.FindBestTour());
                bestOffset = 1;
            }

            for (int i = bestOffset; i < pop.PopulationSize(); i++)
            {
                Tour parent1 = tournament(pop);
                Tour parent2 = tournament(pop);

                if (rand.NextDouble() < crossingFactor)
                {
                    Tour child = crossover(parent1, parent2);
                    newGeneration.saveTour(i, child);
                }
                else
                {
                    Tour child = new Tour(parent1.tour);
                    newGeneration.saveTour(i, child);
                }
            }

            for (int i = bestOffset; i < pop.PopulationSize(); i++)
            {
                mutate(newGeneration.GetTour(i));
            }

            return newGeneration;
        }



        public static Tour crossover(Tour p1, Tour p2)
        {
            Tour child = new Tour();

                int start = (int)(rand.NextDouble() * p1.TourSize());
                int end = (int)(rand.NextDouble() * p1.TourSize());

                for (int i = 0; i < child.TourSize(); i++)
                {
                    if (start < end && i > start && i < end)
                    {
                        child.setTourCity(i, p1.GetTourCity(i));
                    }
                    else if (start > end)
                    {
                        if (!(i < start && i > end))
                        {
                            child.setTourCity(i, p1.GetTourCity(i));
                        }
                    }
                }


                for (int i = 0; i < p2.TourSize(); i++)
                {
                    if (!child.containsCity(p2.GetTourCity(i)))
                    {
                        for (int y = 0; y < child.TourSize(); y++)
                        {
                            if (child.GetTourCity(y) == null)
                            {
                                child.setTourCity(y, p2.GetTourCity(i));
                                break;
                            }
                        }
                    }
                }

                return child;
        }


        public static void mutate(Tour tour)
        {
            for (int i = 0; i < tour.TourSize(); i++)
            {
                if (rand.NextDouble() < mutationFactor)
                {
                    int position = (int)(rand.NextDouble() * tour.TourSize());
                    City c1 = tour.GetTourCity(i);
                    City c2 = tour.GetTourCity(position);
                    tour.setTourCity(position, c1);
                    tour.setTourCity(i, c2);
                }
            }
        }

        public static Tour tournament(Population p)
        {
            Population tournament = new Population(tournamentSize, false);
            for (int i = 0; i < tournamentSize; i++)
            {
                int idx = (int)(rand.NextDouble() * p.PopulationSize());
                tournament.saveTour(i, p.GetTour(idx));
            }
            Tour bestTour = tournament.FindBestTour();
            return bestTour;
        }

        public static Tour rouletteSelection(Population p)
        {
            double fitSum = 0;
            Tour returnTour = new Tour();

            for (int i = 0; i < p.PopulationSize(); i++)
            {
                fitSum += p.GetTour(i).Fitness();
            }

            double number = rand.NextDouble() * fitSum;
            double secondSum = 0;
            for (int i = 0; i < p.PopulationSize(); i++)
            {
                secondSum += p.GetTour(i).Fitness();
                if (secondSum >= number)
                {
                    returnTour = p.GetTour(i);
                    break;
                }
            }

            return returnTour;
        }
    }


}

