﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenAlgComi
{
    class City
    {
        int X { get; set; }
        int Y { get; set; }
        private static Random rnd = new Random();


        public City(int minX, int maX, int minY, int maxY)
        {
            X = rnd.Next(minX, maX);
            Y = rnd.Next(minY, maxY);
        }

        public City(int x, int y)
        {
            X = x;
            Y = y;
        }

        public double DistanceToCity(City city)
        {
            double xDist = Double.Parse(Math.Abs(X - city.X).ToString());
            double yDist = Double.Parse(Math.Abs(Y - city.Y).ToString());
            return Math.Sqrt((Math.Pow(xDist, 2) + Math.Pow(yDist, 2)));
        }
         
        public override string ToString()
        {
            base.ToString();
            return "(" + X + ", " + Y + ")";
        }

        public bool Equals(City city)
        {
            if (city.X == X && city.Y == Y)
                return true;
            else
                return false;
        }
    }
}
