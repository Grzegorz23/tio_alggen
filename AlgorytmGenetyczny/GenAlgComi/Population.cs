﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GenAlgComi
{
    class Population
    {
        List<Tour> tours;

        public Population(int populationSize, bool init)
        {
            tours = new List<Tour>(populationSize);
            if (init)
            {
                // Loop and create individuals
                for (int i = 0; i < populationSize; i++)
                {
                    Tour newTour = new Tour();
                    newTour.generateTour();
                    tours.Add(newTour);
                }
            }
        }

        public void saveTour(int idx, Tour tour)
        {
            tours.Insert(idx, tour);
        }

        public Tour GetTour(int idx)
        {
            if(idx < tours.Count)
                return tours[idx];
            else
            {
                System.Console.WriteLine("Out of bounds");
                return null;
            }
        }

        public double AvgFitness()
        {
            double fitSum = 0;

            for(int i=0;i<tours.Count;i++)
            {
                fitSum += tours[i].Fitness();
            }
            return fitSum / (tours.Count);
        }

        public Tour FindBestTour()
        {
            Tour best = tours[0];
            for (int i = 1; i < tours.Count; i++)
            {
                if (best.Fitness() <= GetTour(i).Fitness())
                {
                    best = GetTour(i);
                }
            }
            return best;
        }

        public int PopulationSize()
        {
            return tours.Count;
        }


    }
}
